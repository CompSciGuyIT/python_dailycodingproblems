########################################################################################################################
# This problem was asked by Bloomberg.
#
# There are N prisoners standing in a circle, waiting to be executed. The executions are carried out starting with
# the kth person, and removing every successive kth person going clockwise until there is no one left.
#
# Given N and k, write an algorithm to determine where a prisoner should stand in order to be the last survivor.
#
# For example, if N = 5 and k = 2, the order of executions would be [2, 4, 1, 5, 3], so you should return 3.
#
# Bonus: Find an O(log N) solution if k = 2.
########################################################################################################################


# N is the length of the list
# k is the starting element
def execution(N, k):
    # Create list
    prisoners = []
    for i in range(1, N + 1):
        prisoners.append(i)

    return last_survivor(prisoners, k)


# Helper function
# Creates a new list by removing the assigned index and iterating the rest of the elements from there
def reduce_list(old_list, position):
    # Get index
    position -= 1

    # Create new list
    result = []
    for i in old_list:
        result.append(old_list[position % len(old_list)])
        position += 1
    result.pop(0)

    return result


# Helper function
# Find last survivor
def last_survivor(prisoners, position):
    for i in range(1, len(prisoners)):
        prisoners = reduce_list(prisoners, position)

    return prisoners.pop()


# Solve problem
print(execution(5, 2))
print(execution(5, 3))
print(execution(12, 3))
print(execution(13, 3))
print(execution(13, 5))
