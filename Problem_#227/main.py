# This problem was asked by Facebook.
#
# Boggle is a game played on a 4 x 4 grid of letters.
# The goal is to find as many words as possible that can be formed by a sequence of adjacent letters in the grid,
# using each cell at most once.
# Given a game board and a dictionary of valid words, implement a Boggle solver.

# Download the word list.
# This only needs to happen once.
# import nltk
#
# nltk.download()

from colorama import Fore
from nltk.corpus import words

rows, columns = 4, 4
left_bound, upper_bound = -1, -1
right_bound, lower_bound = 4, 4
wordlist = words.words()

# Create set to hold unique words that are found
found_words = set()


# Class containing the letter and if its placement in the grid has already been visited
class Square:
    def __init__(self, letter, column, row):
        self.letter = letter
        self.column = column
        self.row = row
        self.seen = False


# Function to check if square has been visited
# and add its letter to the word if it has not.
# Wordlist is then checked.
def check_square(square, my_word):
    # Display grid
    debug_print_grid(grid, square.column, square.row)

    # make deep copy of the string
    this_word = my_word[::-1][::-1]

    # Check if square has been visited
    if square.seen:
        return

    square.seen = True
    if this_word == "":
        this_word += square.letter
    else:
        this_word += square.letter.lower()

    # Add word to found_words if it is in wordlist
    if this_word in wordlist:
        found_words.add(this_word)

    # Check adjacent squares
    # Check upper square
    if square.row - 1 > upper_bound:
        check_square(grid[square.column][square.row - 1], this_word)

    # Check right square
    if square.column + 1 < right_bound:
        check_square(grid[square.column + 1][square.row], this_word)

    # Check lower square
    if square.row + 1 < lower_bound:
        check_square(grid[square.column][square.row + 1], this_word)

    # Check left square
    if square.column - 1 > left_bound:
        check_square(grid[square.column - 1][square.row], this_word)


# Reset squares to unvisited
def reset_squares(my_grid):
    for row in range(0, 4):
        for column in range(0, 4):
            my_grid[column][row].seen = False


# Display the grid
# Set starting square to RED and rest to BLUE
def print_start_grid(my_grid, column, row):
    for grid_row in range(0, 4):
        for grid_column in range(0, 4):
            if grid_column == 3:
                if grid_column == my_grid[column][row].column and grid_row == my_grid[column][row].row:
                    print(Fore.RED + '[' + my_grid[grid_column][grid_row].letter + ']')
                else:
                    print(Fore.BLUE + '[' + my_grid[grid_column][grid_row].letter + ']')
            else:
                if grid_column == my_grid[column][row].column and grid_row == my_grid[column][row].row:
                    print(Fore.RED + '[' + my_grid[grid_column][grid_row].letter + ']', end="")
                else:
                    print(Fore.BLUE + '[' + my_grid[grid_column][grid_row].letter + ']', end="")

    # New line to separate the grids
    print()


# Display the grid
# Set square to be checked to YELLOW, visited squares to GREEN, and unvisited squares to BLUE
def debug_print_grid(my_grid, column, row):
    for grid_row in range(0, 4):
        for grid_column in range(0, 4):
            if grid_column == 3:
                if grid_column == my_grid[column][row].column and grid_row == my_grid[column][row].row:
                    print(Fore.YELLOW + '[' + my_grid[grid_column][grid_row].letter + ']')
                elif my_grid[grid_column][grid_row].seen:
                    print(Fore.GREEN + '[' + my_grid[grid_column][grid_row].letter + ']')
                else:
                    print(Fore.BLUE + '[' + my_grid[grid_column][grid_row].letter + ']')
            else:
                if grid_column == my_grid[column][row].column and grid_row == my_grid[column][row].row:
                    print(Fore.YELLOW + '[' + my_grid[grid_column][grid_row].letter + ']', end="")
                elif my_grid[grid_column][grid_row].seen:
                    print(Fore.GREEN + '[' + my_grid[grid_column][grid_row].letter + ']', end="")
                else:
                    print(Fore.BLUE + '[' + my_grid[grid_column][grid_row].letter + ']', end="")

    # New line to separate the grids
    print()


# Create word grid
grid = [[Square("0", column, row) for column in range(columns)] for row in range(rows)]

grid[0][0] = Square("c", 0, 0)
grid[0][1] = Square("h", 0, 1)
grid[0][2] = Square("a", 0, 2)
grid[0][3] = Square("t", 0, 3)
grid[1][0] = Square("a", 1, 0)
grid[2][0] = Square("r", 2, 0)
grid[3][0] = Square("t", 3, 0)
grid[3][1] = Square("o", 3, 1)
grid[3][2] = Square("o", 3, 2)
grid[3][3] = Square("n", 3, 3)

# Let's play!
for game_row in range(0, 4):
    for game_column in range(0, 4):
        reset_squares(grid)
        word = ""

        # Display grid
        print_start_grid(grid, game_column, game_row)

        check_square(grid[game_column][game_row], word)

print(found_words)

# word = ""
# word += grid[0][0].letter
# word += grid[1][0].letter.lower()
# word += grid[2][0].letter.lower()
#
# if word in wordlist:
#     print("word: ", True)
#
# if "car" in wordlist:
#     print("car: ", True)
#
# if "Aani" in wordlist:
#     print("Aani: ", True)

# for w in wordlist:
#     if w[:2] == 'ch':
#         print(w)
