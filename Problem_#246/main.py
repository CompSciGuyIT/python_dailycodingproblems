# This problem was asked by Dropbox.
#
# Given a list of words, determine whether the words can be chained to form a circle.
# A word X can be placed in front of another word Y in a circle
# if the last character of X is same as the first character of Y.
#
# For example, the words ['chair', 'height', 'racket', 'touch', 'tunic']
# can form the following circle: chair --> racket --> touch --> height --> tunic --> chair
import copy

words = ['chair', 'height', 'racket', 'touch', 'tunic']

result = []

# Check append method
while len(words):
    result.append(words.pop(0))
    print(f"Words: {words}")
    print(f"Result: {result}\n")


# Recursive function to reduce list
def reduce(removed_word, start_word, word_list):
    if not word_list:
        return [removed_word, start_word]
    else:
        for word in words:
            if removed_word[-1] == word[0]:
                return reduce(word_list.pop(word), start_word, word_list)


# Iterate through each starting word in list
for index in range(len(words)):
    my_list = copy.deepcopy(words)
    starting_word = my_list.pop(index)
    print(my_list)
    # result = reduce_list(my_list)
